<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreateProductTest extends TestCase
{
    /** @test */
    public function authenticated_someone_can_create_product()
    {
        $this->login(["admin", "manager"]);
        $countBeforeCreated = Product::count();
        $dataCreate = $this->getDataValid();
        $response = $this->post($this->getStoreProductRoute(), $dataCreate);
        $this->assertDatabaseHas('products', [
            'name' => $dataCreate['name'],
            'price' => $dataCreate['price'],
            'description' => $dataCreate['description']
        ]);
        $countAfterCreated = Product::count();
        $this->assertEquals($countBeforeCreated + 1, $countAfterCreated);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->has('data', fn (AssertableJson $json) =>
                $json->where('name', $dataCreate['name'])->etc()
            )->etc());
    }

    /** @test */
    public function unauthenticated_someone_can_not_create_product_if_data_exist()
    {
        $dataCreate = $this->getDataValid();
        $response = $this->post($this->getStoreProductRoute(), $dataCreate);
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function authenticated_someone_can_not_create_product_if_name_is_null()
    {
        $this->login(["admin", "manager"]);
        $dataCreate = $this->getDataValid(['name' => null]);
        $response = $this->postJson($this->getStoreProductRoute(), $dataCreate);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->has('errors', fn (AssertableJson $json) =>
                $json->has('name')->etc()
            )->etc());
    }

    /** @test */
    public function authenticated_someone_can_not_create_product_if_price_is_null()
    {
        $this->login(["admin", "manager"]);
        $dataCreate = $this->getDataValid(['price' => null]);
        $response = $this->postJson($this->getStoreProductRoute(), $dataCreate);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->has('errors', fn (AssertableJson $json) =>
                $json->has('price')->etc()
            )->etc());
    }

    /** @test */
    public function authenticated_someone_can_not_create_product_if_category_id_is_null()
    {
        $this->login(["admin", "manager"]);
        $dataCreate = $this->getDataValid(['category_id' => null]);
        $response = $this->postJson($this->getStoreProductRoute(), $dataCreate);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->has('errors', fn (AssertableJson $json) =>
                $json->has('category_id')->etc()
            )->etc());
    }

    /** @test */
    public function authenticated_someone_can_not_create_product_if_data_is_invalid()
    {
        $this->login(["admin", "manager"]);
        $dataCreate = $this->getDataValid([
            'name' => '',
            'price' => '',
            'description' => '',
            'category_id' => ''
        ]);
        $response = $this->postJson($this->getStoreProductRoute(), $dataCreate);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->has('errors', fn (AssertableJson $json) =>
                $json->has('name')->has('price')->has('category_id')->etc())->etc());
    }

    public function getStoreProductRoute()
    {
        return route('products.store');
    }

    public function getDataValid($dataCategories = [])
    {
        $dataCategoriesFake = [
            'name' => $this->faker->text(25),
            'price' => rand(1000000, 20000000),
            'description' => $this->faker->text,
            'category_id' => 10
        ];
        return array_merge($dataCategoriesFake, $dataCategories);
    }
}
