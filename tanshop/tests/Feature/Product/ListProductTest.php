<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class ListProductTest extends TestCase
{
    /** @test */
    public function authenticated_someone_can_get_list_product()
    {
        $this->login(["admin", "manager"]);
        $product = Product::factory()->create()->toArray();
        $response = $this->get($this->getListProductRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.index');
    }

    /** @test */
    public function unauthenticated_someone_can_not_get_list_product()
    {
        $product = Product::factory()->create()->toArray();
        $response = $this->get($this->getListProductRoute());
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
    }

    public function getListProductRoute()
    {
        return route('products.index');
    }
}
