<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class EditProductTest extends TestCase
{
    /** @test */
    public function authenticated_someone_can_edit_product()
    {
        $this->login(["admin", "manager"]);
        $product = Product::factory()->create();
        $dataUpdate = $this->getDataValid();
        $response = $this->post($this->getUpdateProductRoute($product->id), $dataUpdate);
        $this->assertDatabaseHas('products', [
            'name' => $dataUpdate['name'],
            'price' => $dataUpdate['price'],
            'description' => $dataUpdate['description'],
        ]);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->has('data', fn (AssertableJson $json) =>
                $json->where('name', $dataUpdate['name'])->etc()
            )->etc());
    }

    /** @test */
    public function unauthenticated_someone_can_not_edit_product()
    {
        $product = Product::factory()->create();
        $dataUpdate = $this->getDataValid();
        $response = $this->post($this->getUpdateProductRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_someone_can_not_edit_product_if_name_is_null()
    {
        $this->login(["admin", "manager"]);
        $product = Product::factory()->create();
        $dataUpdate = $this->getDataValid(['name' => null]);
        $response = $this->postJson($this->getUpdateProductRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->has('errors', fn (AssertableJson $json) =>
                $json->has('name'))->etc());
    }

    /** @test */
    public function authenticated_someone_can_not_edit_product_if_price_is_null()
    {
        $this->login(["admin", "manager"]);
        $product = Product::factory()->create();
        $dataUpdate = $this->getDataValid(['price' => null]);
        $response = $this->postJson($this->getUpdateProductRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->has('errors', fn (AssertableJson $json) =>
                $json->has('price'))->etc());
    }

    /** @test */
    public function authenticated_someone_can_not_edit_product_if_category_id_is_null()
    {
        $this->login(["admin", "manager"]);
        $product = Product::factory()->create();
        $dataUpdate = $this->getDataValid(['category_id' => null]);
        $response = $this->postJson($this->getUpdateProductRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->has('errors', fn (AssertableJson $json) =>
                $json->has('category_id'))->etc());
    }

    /** @test */
    public function authenticated_someone_can_not_edit_product_if_data_is_invalid()
    {
        $this->login(["admin", "manager"]);
        $product = Product::factory()->create();
        $dataUpdate = $this->getDataValid(['name' => null, 'price' => null, 'description' => null, 'category_id' => null]);
        $response = $this->postJson($this->getUpdateProductRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->has('errors', fn (AssertableJson $json) =>
                $json->has('name')->has('price')->has('category_id'))->etc());
    }

    /** @test */
    public function authenticated_someone_can_not_edit_product_if_product_not_exist_and_data_valid()
    {
        $this->login(["admin", "manager"]);
        $productId = -1;
        $dataUpdate = $this->getDataValid();
        $response = $this->postJson($this->getUpdateProductRoute($productId), $dataUpdate);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function getUpdateProductRoute($id) : string
    {
        return route('products.update', $id);
    }

    public function getDataValid($dataCategories = []) : array
    {
        $dataCategoriesFake = [
            'name' => $this->faker->text(25),
            'price' => rand(1000000, 50000000),
            'description' => $this->faker->text,
            'category_id' => 10
        ];
        return array_merge($dataCategoriesFake, $dataCategories);
    }
}
