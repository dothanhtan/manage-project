<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class DeleteProductTest extends TestCase
{
    /** @test */
    public function authenticated_someone_can_delete_product()
    {
        $this->login([["admin", "manager"]]);
        $product = Product::factory()->create();
        $countProductBefore = Product::count();
        $response = $this->get($this->getDeleteProductRoute($product->id));
        $this->assertDatabaseMissing('products', ['id' => $product->id]);
        $response->assertStatus(Response::HTTP_OK);
        $countProductAfter = Product::count();
        $this->assertEquals($countProductBefore - 1, $countProductAfter);
        $response->assertJson(fn (AssertableJson $json) => $json->has('success'));
    }

    /** @test */
    public function unauthenticated_someone_can_not_delete_product()
    {
        $product = Product::factory()->create();
        $response = $this->get($this->getDeleteProductRoute($product->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    public function getDeleteProductRoute($id)
    {
        return route('products.destroy', $id);
    }
}
