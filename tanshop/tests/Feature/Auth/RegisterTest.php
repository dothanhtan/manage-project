<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    /** @test */
    public function user_can_view_register()
    {
        $response = $this->get($this->registerRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('auth.register');
        $response->assertSee('register');
    }

    /** @test */
    public function user_can_register()
    {
        $dataRegister = User::factory()->make(['password-confirm' => '$2y$10$UWG.kMuwEpXo0RX1.LBgJ.ywQrDbI7CU9V3LzdyHipYrLx03T.2eq'])->toArray();
        $response = $this->post($this->registerRoute(), $dataRegister);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/');
    }

    /** @test */
    public function user_can_not_register_if_name_is_null()
    {
        $dataRegister = User::factory()->make(['name' => null])->toArray();
        $response = $this->post($this->registerRoute(), $dataRegister);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function user_can_not_register_if_phone_is_null()
    {
        $dataRegister = User::factory()->make(['phone' => null])->toArray();
        $response = $this->post($this->registerRoute(), $dataRegister);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['phone']);
    }

    /** @test */
    public function user_can_not_register_if_email_is_null()
    {
        $dataRegister = User::factory()->make(['email' => null])->toArray();
        $response = $this->post($this->registerRoute(), $dataRegister);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function user_can_not_register_if_password_is_null()
    {
        $dataRegister = User::factory()->make([ 'password' => ''])->toArray();
        $response = $this->post($this->registerRoute(), $dataRegister);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['password']);
    }

    public function registerRoute()
    {
        return route('register');
    }
}
