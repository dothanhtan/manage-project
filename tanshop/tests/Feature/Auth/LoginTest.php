<?php

namespace Tests\Feature\Auth;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use WithFaker;

    /** @test */
    public function user_can_view_login()
    {
        $response = $this->get($this->loginRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('auth.login');
        $response->assertSee('login');
    }

    /** @test */
    public function user_can_login()
    {
        $dataLogin = $this->getDataValid(['email'=>'admin@gmail.com', 'password' => '$2y$10$8RJHgsnqz6fLTrnKK1Ezv.C3kggKnUoLXfXNqIl.kZjzL4r0hFyNy']);
        $response = $this->post($this->loginRoute(), $dataLogin);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('users', ['email' => $dataLogin['email'], 'password' => $dataLogin['password']]);
        $response->assertRedirect('/');
    }

    /** @test */
    public function user_can_not_login()
    {
        $dataLogin = $this->getDataValid();
        $response = $this->post($this->loginRoute(), $dataLogin);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseMissing('users', ['email' => $dataLogin['email'], 'password' => $dataLogin['password']]);
    }

    /** @test */
    public function user_can_not_login_if_email_is_null()
    {
        $dataLogin = $this->getDataValid(['email'=>'']);
        $response = $this->post($this->loginRoute(), $dataLogin);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function user_can_not_login_if_password_is_null()
    {
        $dataLogin = $this->getDataValid(['password'=>'']);
        $response = $this->post($this->loginRoute(), $dataLogin);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['password']);
    }

    public function loginRoute()
    {
        return route('login');
    }

    public function getDataValid($dataLogin = [])
    {
        $dataLoginUsersFake =
            [
                'email'     => $this->faker->unique()->safeEmail(),
                'password'  => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            ];
        return array_merge($dataLoginUsersFake, $dataLogin);
    }
}
