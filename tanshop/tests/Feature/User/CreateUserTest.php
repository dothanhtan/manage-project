<?php

namespace Tests\Feature\User;

use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Tests\TestCase;

class CreateUserTest extends TestCase
{
    /** @test */
    public function authenticated_someone_can_store_user()
    {
        $this->login(["manager"]);
        $user = $this->getDataValid();
        $countBeforeCreate = User::count();
        $response = $this->post($this->getStoreUserRoute(), $user);
        $this->assertDatabaseHas('users', [
            'name' => $user['name'],
            'phone' => $user['phone'],
            'email' => $user['email']
        ]);
        $countAfterCreate = User::count();
        $this->assertEquals($countBeforeCreate + 1, $countAfterCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('users.index'));
    }

    /** @test */
    public function authenticated_someone_can_not_store_user_if_name_is_null()
    {
        $this->login(["manager"]);
        $user = User::factory()->make(['name' => null])->toArray();
        $response = $this->post($this->getStoreUserRoute(), $user);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticated_someone_can_not_store_user_if_phone_is_null()
    {
        $this->login(["manager"]);
        $user = User::factory()->make(['phone' => null])->toArray();
        $response = $this->post($this->getStoreUserRoute(), $user);
        $response->assertSessionHasErrors(['phone']);
    }

    /** @test */
    public function authenticated_someone_can_not_store_user_if_email_is_null()
    {
        $this->login(["manager"]);
        $user = User::factory()->make(['email' => null])->toArray();
        $response = $this->post($this->getStoreUserRoute(), $user);
        $response->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function authenticated_someone_can_not_store_user_if_password_is_null()
    {
        $this->login(["manager"]);
        $user = User::factory()->make(['password' => null])->toArray();
        $response = $this->post($this->getStoreUserRoute(), $user);
        $response->assertSessionHasErrors(['password']);
    }

    /** @test */
    public function authenticated_someone_can_not_store_user_if_data_is_invalid()
    {
        $this->login(["manager"]);
        $dataUser = $this->getDataValid(
            [
                'name' => null,
                'phone' => null,
                'email' => null,
                'password' => null,
                'password_confirmation' => null
            ]);
        $response = $this->post($this->getStoreUserRoute(), $dataUser);
        $response->assertSessionHasErrors(['name', 'phone', 'email', 'password', 'password_confirmation']);
    }

    /** @test */
    public function unauthenticated_someone_can_not_create_user()
    {
        $response = $this->get($this->getCreateUserRoute());
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function unauthenticated_someone_can_not_store_user_if_data_valid()
    {
        $dataValid = $this->getDataValid();
        $response = $this->from($this->getCreateUserRoute())->post($this->getStoreUserRoute(), $dataValid);
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
    }

    public function getCreateUserRoute()
    {
        return route('users.create');
    }

    public function getStoreUserRoute()
    {
        return route('users.store');
    }

    public function getDataValid($dataUsers = [])
    {
        $dataUsersFake = [
            'name' => $this->faker->name(),
            'phone' => 0 . rand(100000000, 999999999),
            'email' => $this->faker->unique()->safeEmail(),
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'password_confirmation' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'remember_token' => Str::random(10),
        ];
        return array_merge($dataUsersFake, $dataUsers);
    }
}
