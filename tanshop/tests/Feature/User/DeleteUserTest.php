<?php

namespace Tests\Feature\User;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteUserTest extends TestCase
{
    /** @test */
    public function authenticated_someone_can_delete_user()
    {
        $this->login(["admin"]);
        $user = User::factory()->create();
        $countBeforeDelete = User::count();
        $response = $this->delete($this->getDeleteUserRoute($user->id));
        $this->assertDatabaseMissing('users', ['id' => $user->id]);
        $countAfterDelete = User::count();
        $this->assertEquals($countBeforeDelete - 1, $countAfterDelete);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('users.index'));
    }

    /** @test */
    public function unauthenticated_someone_can_not_delete_user()
    {
        $user = User::factory()->create();
        $response = $this->delete($this->getDeleteUserRoute($user->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    public function getDeleteUserRoute($id)
    {
        return route('users.destroy', $id);
    }
}
