<?php

namespace Tests\Feature\User;

use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class ShowUserTest extends TestCase
{
    /** @test */
    public function authenticated_someone_can_show_user()
    {
        $this->login(["admin"]);
        $user = User::factory()->create();
        $response = $this->get($this->getShowUserRoute($user->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.show');
    }

    /** @test */
    public function unauthenticated_someone_can_not_show_user()
    {
        $user = User::factory()->create();
        $response = $this->get($this->getShowUserRoute($user->id));
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
    }

    public function getShowUserRoute($id)
    {
        return route('users.show', $id);
    }
}
