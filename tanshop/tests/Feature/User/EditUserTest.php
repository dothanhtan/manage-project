<?php

namespace Tests\Feature\User;

use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class EditUserTest extends TestCase
{
    /** @test */
    public function authenticated_someone_can_update_user()
    {
        $this->login(["manager"]);
        $user = User::factory()->create();
        $data = User::factory()->make()->toArray();
        $response = $this->put($this->getUpdateUserRoute($user->id), $data);
        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'name' => $data['name'],
            'phone' => $data['phone'],
            'email' => $data['email']
        ]);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('users.index'));
    }

    /** @test */
    public function unauthenticated_someone_can_not_update_user_if_data_is_valid()
    {
        $user = User::factory()->create();
        $data = User::factory()->make()->toArray();
        $response = $this->put($this->getUpdateUserRoute($user->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_someone_can_not_update_user_if_name_is_null()
    {
        $this->login(["manager"]);
        $user = User::factory()->create();
        $data = User::factory()->make(['name' => null])->toArray();
        $response = $this->put($this->getUpdateUserRoute($user->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticated_someone_can_not_update_user_if_phone_is_null()
    {
        $this->login(["manager"]);
        $user = User::factory()->create();
        $data = User::factory()->make(['phone' => null])->toArray();
        $response = $this->put($this->getUpdateUserRoute($user->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['phone']);
    }

    /** @test */
    public function authenticated_someone_can_not_update_user_if_email_is_null()
    {
        $this->login(["manager"]);
        $user = User::factory()->create();
        $data = User::factory()->make(['email' => null])->toArray();
        $response = $this->put($this->getUpdateUserRoute($user->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function authenticated_someone_can_not_update_user_if_password_is_null()
    {
        $this->login(["manager"]);
        $user = User::factory()->create();
        $data = User::factory()->make(['password'=> null, 'changePassword' => 'on'])->toArray();
        $response = $this->put($this->getUpdateUserRoute($user->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['password']);
    }

    /** @test */
    public function authenticated_someone_can_not_update_user_if_password_confirmation_is_null()
    {
        $this->login(["manager"]);
        $user = User::factory()->create();
        $data = User::factory()->make(['password_confirmation' => null, 'changePassword' => 'on'])->toArray();
        $response = $this->put($this->getUpdateUserRoute($user->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['password_confirmation']);
    }

    /** @test */
    public function authenticated_someone_can_not_update_user_if_data_is_invalid()
    {
        $this->login(["manager"]);
        $user = User::factory()->create();
        $data = [
            'name' => null,
            'phone' => null,
            'email' => null,
            'password' => null,
            'password_confirmation' => null,
            'changePassword' => 'on'
        ];
        $response = $this->put($this->getUpdateUserRoute($user->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name' ,'phone' ,'email' ,'password', 'password_confirmation']);
    }

    public function getUpdateUserRoute($id)
    {
        return route('users.update', $id);
    }
}
