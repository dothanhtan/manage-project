<?php

namespace Tests\Feature\User;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class ListUserTest extends TestCase
{
    /** @test */
    public function authenticated_someone_can_get_list_user()
    {
        $this->login(["manager"]);
        $user = User::factory()->create();
        $response = $this->get($this->getListUserRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.index');
    }

    /** @test */
    public function unauthenticated_someone_can_not_list_user()
    {
        $response = $this->get($this->getListUserRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_someone_can_search_list_user()
    {
        $this->login(["manager"]);
        $user = $this->getDataValid(['name' => 'Do Thanh Tan']);
        $response = $this->get($this->getListUserRoute(), $user);
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas('users', ['name' => $user['name']]);
        $response->assertSee(['name']);
        $response->assertViewIs('users.index');
    }

    /** @test */
    public function authenticated_can_not_search_list_user()
    {
        $this->login(["manager"]);
        $user = $this->getDataValid();
        $response = $this->get($this->getListUserRoute(), $user);
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseMissing('users', ['name' => $user['name']]);
        $response->assertSee(['name']);
        $response->assertViewIs('users.index');
    }

    public function getDataValid($dataSearchUsers = [])
    {
        $dataSearchUsersFake = [
            'name' => $this->faker->name,
            'email' => '',
            'role_name'=> ''
        ];
        return array_merge($dataSearchUsersFake, $dataSearchUsers);
    }

    public function getListUserRoute()
    {
        return route('users.index');
    }
}
