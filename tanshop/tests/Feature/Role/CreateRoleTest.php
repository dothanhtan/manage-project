<?php

namespace Tests\Feature\Role;

use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Tests\TestCase;

class CreateRoleTest extends TestCase
{
    /** @test */
    public function authenticated_someone_can_create_role()
    {
//        $this->withoutExceptionHandling();
        $this->login(["admin", "manager"]);
        $roleBeforeCreate = Role::count();
        $role = $this->getDataValid();
        $response = $this->post($this->getCreateRoleRoute(), $role);
        $this->assertDatabaseHas('roles', $role);
        $roleAfterCreate = Role::count();
        $this->assertEquals($roleBeforeCreate + 1, $roleAfterCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('roles.index'));
    }

    /** @test */
    public function authenticated_someone_can_not_create_role_if_name_null()
    {
        $this->login(["admin", "manager"]);
        $dataCreate = Role::factory()->make(['name' => null])->toArray();
        $response = $this->post($this->getCreateRoleRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticated_someone_can_not_create_role_if_display_name_null()
    {
        $this->login(["admin", "manager"]);
        $dataCreate = Role::factory()->make(['display_name' => null])->toArray();
        $response = $this->post($this->getCreateRoleRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['display_name']);
    }

    /** @test */
    public function authenticated_someone_can_not_create_role_if_data_is_invalid()
    {
        $this->login(["admin", "manager"]);
        $dataRole = $this->getDataValid(['name' => null, 'display_name' => null]);
        $response = $this->post($this->getCreateRoleRoute(), $dataRole);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name', 'display_name']);
    }

    /** @test */
    public function unauthenticated_someone_can_not_create_role()
    {

        $role = $this->getDataValid();
        $response = $this->post($this->getCreateRoleRoute(), $role);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    public function getCreateRoleRoute()
    {
        return route('roles.store');
    }

    public function getDataValid($dataRoles = [])
    {
        $dataRolesFake = [
            'name' => Str::lower($this->faker->text(10)),
            'display_name' => $this->faker->text(20)
        ];
        return array_merge($dataRolesFake, $dataRoles);
    }
}
