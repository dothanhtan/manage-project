<?php

namespace Tests\Feature\Role;

use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Tests\TestCase;

class EditRoleTest extends TestCase
{
    /** @test */
    public function authenticated_someone_can_edit_role()
    {
        $this->login(["manager"]);
        $role = Role::factory()->create();
        $dataValid = $this->getDataValid();
        $response = $this->put($this->getEditRoleRoute($role->id), $dataValid);
        $this->assertDatabaseHas('roles', $dataValid);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('roles.index'));
    }

    /** @test */
    public function unauthenticated_someone_can_not_edit_role()
    {
        $role = Role::factory()->create();
        $data = $this->getDataValid();
        $response = $this->put($this->getEditRoleRoute($role->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticate_someone_can_not_edit_role_if_name_is_null()
    {
        $this->login(["manager"]);
        $role = Role::factory()->create();
        $dataRole = Role::factory()->make(['name' => null])->toArray();
        $response = $this->put($this->getEditRoleRoute($role->id), $dataRole);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }
    /** @test */
    public function authenticate_someone_can_not_edit_role_if_display_name_is_null()
    {
        $this->login(["manager"]);
        $role = Role::factory()->create();
        $dataRole = Role::factory()->make(['display_name' => null])->toArray();
        $response = $this->put($this->getEditRoleRoute($role->id), $dataRole);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['display_name']);
    }

    /** @test */
    public function authenticate_someone_can_not_edit_role_if_data_is_null()
    {
        $this->login(["manager"]);
        $role = Role::factory()->create();
        $dataRole = $this->getDataValid(['name' => null ,'display_name' => null]);
        $response = $this->put($this->getEditRoleRoute($role->id), $dataRole);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name', 'display_name']);
    }

    public function getEditRoleRoute($id)
    {
        return route('roles.update', $id);
    }

    public function getEditRoleFormRoute($id)
    {
        return route('roles.edit', $id);
    }

    public function getDataValid($dataRoles = [])
    {
        $dataRolesFake = [
            'name' => Str::lower($this->faker->text(10)),
            'display_name' => $this->faker->text(20)
        ];
        return array_merge($dataRolesFake, $dataRoles);
    }
}
