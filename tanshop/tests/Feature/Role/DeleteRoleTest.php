<?php

namespace Tests\Feature\Role;

use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteRoleTest extends TestCase
{
    /** @test*/
    public function authenticated_someone_can_delete_role()
    {
        $this->login(["manager"]);
        $role = Role::factory()->create();
        $roleBeforeDelete = Role::count();
        $response = $this->delete($this->getDeleteRoleRoute($role->id));
        $roleAfterDelete = Role::count();
        $this->assertEquals($roleBeforeDelete - 1, $roleAfterDelete);
        $this->assertDatabaseMissing('roles', ['id' => $role->id]);
        $roleAfterDelete = Role::count();
        $this->assertEquals($roleBeforeDelete - 1, $roleAfterDelete);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('roles.index'));
    }

    /** @test*/
    public function authenticated_someone_can_not_delete_role_if_role_not_exist()
    {
        $this->login(["manager"]);
        $idRole = -1;
        $response = $this->delete($this->getDeleteRoleRoute($idRole));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function unauthenticated_someone_can_not_delete_role()
    {
        $role = Role::factory()->create();
        $response = $this->delete($this->getDeleteRoleRoute($role->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    public function getDeleteRoleRoute($id)
    {
        return route('roles.destroy', $id);
    }
}
