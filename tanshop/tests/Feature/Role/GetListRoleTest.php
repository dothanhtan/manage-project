<?php

namespace Tests\Feature\Role;

use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListRoleTest extends TestCase
{
    /** @test */
    public function authenticated_someone_can_get_list_role()
    {
        $this->login(['manager']);
        $role = Role::factory()->create()->toArray();
        $response = $this->get($this->getRoleRoute(), $role);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.index');
    }

    /** @test */
    public function unauthenticated_someone_can_not_get_list_role()
    {
        $response = $this->get($this->getRoleRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_someone_can_role_list_search()
    {
        $this->login(["manager"]);
        $role = Role::factory()->make(['name' => 'manager'])->toArray();
        $response = $this->get($this->getRoleRoute(), $role);
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas('roles', ['name' => $role['name']]);
        $response->assertSee(['name']);
        $response->assertViewIs('roles.index');
    }

    /** @test */
    public function authenticated_someone_can_not_role_list_search()
    {
        $this->login(["manager"]);
        $role = Role::factory()->make()->toArray();
        $response = $this->get($this->getRoleRoute(), $role);
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseMissing('roles', $role);
        $response->assertSee(['name']);
        $response->assertViewIs('roles.index');
    }

    public function getRoleRoute()
    {
        return route('roles.index');
    }
}
