<?php

namespace Tests\Feature\Role;

use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class ShowRoleTest extends TestCase
{
    /** @test */
    public function authenticated_someone_can_get_show_role()
    {
        $this->login(["admin", "manager"]);
        $role = Role::factory()->create();
        $response = $this->get($this->getShowRoleRoute($role->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.show');
    }

    /** @test */
    public function authenticated_someone_can_not_get_show_role()
    {
        $this->login(["admin", "manager"]);
        $roleId = -1;
        $response = $this->get($this->getShowRoleRoute($roleId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function unauthenticated_someone_can_not_get_show_role()
    {
        $role = Role::factory()->create();
        $response = $this->get($this->getShowRoleRoute($role->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    public function getShowRoleRoute($id)
    {
        return route('roles.show', $id);
    }
}
