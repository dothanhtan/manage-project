<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_someone_can_create_category()
    {
        $this->login(["admin", "manager"]);
        $countBeforeCreated = Category::count();
        $category = ['name' => $this->faker->text(25)];
        $response = $this->post($this->getStoreCategoryRoute(), $category);
        $this->assertDatabaseHas('categories', $category);
        $countAfterCreated = Category::count();
        $this->assertEquals($countBeforeCreated + 1, $countAfterCreated);
        $response->assertRedirect(route('categories.index'));
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function unauthenticated_someone_can_create_category_if_data_valid()
    {
        $category = ['name' => $this->faker->text(25)];
        $response = $this->post($this->getStoreCategoryRoute(), $category);
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function authenticated_someone_can_not_create_category_if_name_is_null()
    {
        $this->login(["admin", "manager"]);
        $category = ['name' => null];
        $response = $this->post($this->getStoreCategoryRoute(), $category);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticated_someone_can_see_create_category_form()
    {
        $this->login(["admin", "manager"]);
        $response = $this->get($this->getCreateCategoryRoute());
        $response->assertViewIs('categories.create');
    }

    /** @test */
    public function authenticated_someone_can_see_name_required_text_if_name_is_null()
    {
        $this->login(["admin", "manager"]);
        $category = ['name' => null];
        $response = $this->from($this->getCreateCategoryRoute())->post($this->getStoreCategoryRoute(), $category);
        $response->assertRedirect($this->getCreateCategoryRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function unauthenticated_someone_can_not_see_create_category_form()
    {
        $response = $this->get($this->getCreateCategoryRoute());
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
    }

    public function getStoreCategoryRoute()
    {
        return route('categories.store');
    }

    public function getCreateCategoryRoute()
    {
        return route('categories.create');
    }
}
