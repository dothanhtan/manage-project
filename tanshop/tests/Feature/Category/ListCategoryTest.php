<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class ListCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_someone_can_get_list_category()
    {
        $this->login(["admin", "manager"]);
        $category = Category::factory()->create();
        $response = $this->get($this->getListCategoryRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.index');
    }

    /** @test */
    public function unauthenticated_someone_can_not_get_list_category()
    {
        $category = Category::factory()->create();
        $response = $this->get($this->getListCategoryRoute());
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
    }

    public function getListCategoryRoute()
    {
        return route('categories.index');
    }
}
