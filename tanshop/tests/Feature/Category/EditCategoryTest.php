<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class EditCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_someone_can_update_category_if_data_valid()
    {
        $this->login(["admin", "manager"]);
        $category = Category::factory()->create();
        $dataUpdated = ['name' => $this->faker->text(25)];
        $response = $this->put($this->getUpdateCategoryRoute($category->id), $dataUpdated);
        $this->assertDatabaseHas('categories', ['id' => $category->id, 'name' => $dataUpdated['name']]);
        $response->assertRedirect(route('categories.index'));
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function unauthenticated_someone_can_not_update_category_if_data_valid()
    {
        $category = Category::factory()->create();
        $dataUpdated = ['name' => $this->faker->text(25)];
        $response = $this->put($this->getUpdateCategoryRoute($category->id), $dataUpdated);
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function authenticated_someone_can_not_update_category_if_category_not_exist()
    {
        $this->login(["admin", "manager"]);
        $categoryID = -1;
        $dataUpdated = ['name' => $this->faker->text(25)];
        $response = $this->put($this->getUpdateCategoryRoute($categoryID), $dataUpdated);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function authenticated_someone_can_not_update_category_if_name_is_null()
    {
        $this->login(["admin", "manager"]);
        $category = Category::factory()->create();
        $dataUpdated = ['name' => null];
        $response = $this->put($this->getUpdateCategoryRoute($category->id), $dataUpdated);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticated_someone_can_see_update_category_form()
    {
        $this->login(["admin", "manager"]);
        $category = Category::factory()->create();
        $response = $this->get($this->getEditCategoryRoute($category->id));
        $response->assertViewIs('categories.edit');
    }

    /** @test */
    public function unauthenticated_someone_can_not_see_update_category_form()
    {
        $category = Category::factory()->create();
        $response = $this->get($this->getEditCategoryRoute($category->id));
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function authenticated_someone_can_see_name_required_text_when_name_is_null()
    {
        $this->login(["admin", "manager"]);
        $category = Category::factory()->create();
        $dataUpdated = ['name' => null];
        $response = $this->from($this->getEditCategoryRoute($category->id))
            ->put($this->getUpdateCategoryRoute($category->id), $dataUpdated);
        $response->assertRedirect($this->getEditCategoryRoute($category->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    public function getUpdateCategoryRoute($id)
    {
        return route('categories.update', $id);
    }

    public function getEditCategoryRoute($id)
    {
        return route('categories.edit', $id);
    }
}
