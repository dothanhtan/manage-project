<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_someone_can_delete_category()
    {
        $this->login(["admin", "manager"]);
        $category = Category::factory()->create();
        $countBeforeDelete = Category::count();
        $response = $this->delete($this->getDeleteCategoryRoute($category->id));
        $this->assertDatabaseMissing('categories', ['id' => $category->id]);
        $countAfterDelete = Category::count();
        $this->assertEquals($countBeforeDelete - 1, $countAfterDelete);
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function authenticated_someone_can_not_delete_category_if_category_not_exist()
    {
        $this->login(["admin", "manager"]);
        $categoryID = -1;
        $response = $this->delete($this->getDeleteCategoryRoute($categoryID));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function unauthenticated_someone_can_not_delete_category()
    {
        $category = Category::factory()->create();
        $response = $this->delete($this->getDeleteCategoryRoute($category->id));
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
    }

    public function getDeleteCategoryRoute($id)
    {
        return route('categories.destroy', $id);
    }
}
