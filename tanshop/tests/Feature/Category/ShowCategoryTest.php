<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class ShowCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_someone_can_show_category()
    {
        $this->login(["admin", "manager"]);
        $category = Category::factory()->create();
        $response = $this->get($this->getShowCategoryRoute($category->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.show');
    }

    /** @test */
    public function unauthenticated_someone_can_not_show_category()
    {
        $category = Category::factory()->create();
        $response = $this->get($this->getShowCategoryRoute($category->id));
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function authenticated_someone_can_not_show_category_if_category_not_exist()
    {
        $this->login(["admin", "manager"]);
        $categoryID = -1;
        $response = $this->get($this->getShowCategoryRoute($categoryID));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function getShowCategoryRoute($id)
    {
        return route('categories.show', $id);
    }
}
