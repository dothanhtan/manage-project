<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('roles')->truncate();
        DB::table('roles')->insert([
            [
                'name' => 'admin',
                'display_name' => 'Quản trị viên',
                'created_at' => Carbon::now()
            ],
            [
                'name' => 'manager',
                'display_name' => 'Người quản lý',
                'created_at' => Carbon::now()
            ],
            [
                'name' => 'user',
                'display_name' => 'Khách hàng',
                'created_at' => Carbon::now()
            ]
        ]);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
