<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions =
            [
                'role-list' => 'Liệt kê vai trò',
                'role-create' => 'Tạo vai trò',
                'role-edit' => 'Sửa vai trò',
                'role-delete' => 'Xóa vai trò',
                'product-list' => 'Liệt kê sản phẩm',
                'product-create' => 'Tạo sản phẩm',
                'product-edit' => 'Sửa sản phẩm',
                'product-delete' => 'Xóa sản phẩm',
                'user-list' => 'Liệt kê khách hàng',
                'user-create' => 'Tạo khách hàng',
                'user-edit' => 'Sửa khách hàng',
                'user-delete' => 'Xóa khách hàng',
                'category-list' => 'Liệt kê hạng mục',
                'category-create' => 'Tạo hạng mục',
                'category-edit' => 'Sửa hạng mục',
                'category-delete' => 'Xóa hạng mục',
            ];

        foreach ($permissions as $name => $permission)
        {
            Permission::updateOrCreate(['name' => $name], ['display_name' => $permission]);
        }
    }
}
