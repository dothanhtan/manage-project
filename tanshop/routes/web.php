<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::middleware(['auth'])->name('roles.')->controller(RoleController::class)->prefix('/roles')->group(function ()
{
    Route::get('/', 'index')->name('index')
        ->middleware('check.permission:role-list');

    Route::get('/create', 'create')->name('create')
        ->middleware('check.permission:role-create');

    Route::post('/store', 'store')->name('store')
        ->middleware('check.permission:role-create');

    Route::get('/{id}/show', 'show')->name('show')
        ->middleware('check.permission:role-list');

    Route::get('/{id}/edit', 'edit')->name('edit')
        ->middleware('check.permission:role-edit');

    Route::put('/{id}/update', 'update')->name('update')
        ->middleware('check.permission:role-edit');

    Route::delete('/{id}/delete', 'destroy')->name('destroy')
        ->middleware('check.permission:role-delete');
});

Route::middleware(['auth'])->name('users.')->controller(UserController::class)->prefix('/users')->group(function ()
{
    Route::get('/', 'index')->name('index')
        ->middleware('check.permission:user-list');

    Route::get('/create', 'create')->name('create')
        ->middleware('check.permission:user-create');

    Route::post('/store', 'store')->name('store')
        ->middleware('check.permission:user-create');

    Route::get('/show/{id}', 'show')->name('show')
        ->middleware('check.permission:user-list');

    Route::get('/edit/{id}', 'edit')->name('edit')
        ->middleware('check.permission:user-edit');

    Route::put('/update/{id}', 'update')->name('update')
        ->middleware('check.permission:user-edit');

    Route::delete('/delete/{id}', 'destroy')->name('destroy')
        ->middleware('check.permission:user-delete');
});

Route::middleware('auth')->name('products.')->controller(ProductController::class)->prefix('/products')->group(function ()
{
    Route::get('/', 'index')->name('index')
        ->middleware('check.permission:product-list');

    Route::get('/create', 'create')->name('create')
        ->middleware('check.permission:product-create');

    Route::post('/store', 'store')->name('store')
        ->middleware('check.permission:product-create');

    Route::get('/show/{id}', 'show')->name('show')
        ->middleware('check.permission:product-list');

    Route::get('/edit/{id}', 'edit')->name('edit')
        ->middleware('check.permission:product-edit');

    Route::post('/update/{id}', 'update')->name('update')
        ->middleware('check.permission:product-edit');

    Route::get('/delete/{id}', 'destroy')->name('destroy')
        ->middleware('check.permission:product-delete');
});

Route::middleware('auth')->name('categories.')->controller(CategoryController::class)->prefix('/categories')->group(function ()
{
    Route::get('/', 'index')->name('index')
        ->middleware('check.permission:category-list');

    Route::get('/create', 'create')->name('create')
        ->middleware('check.permission:category-create');

    Route::post('/store', 'store')->name('store')
        ->middleware('check.permission:category-create');

    Route::get('/show/{id}', 'show')->name('show')
        ->middleware('check.permission:category-list');

    Route::get('/edit/{id}', 'edit')->name('edit')
        ->middleware('check.permission:category-edit');

    Route::put('/update/{id}', 'update')->name('update')
        ->middleware('check.permission:category-edit');

    Route::delete('/delete/{id}', 'destroy')->name('destroy')
        ->middleware('check.permission:category-delete');
});
