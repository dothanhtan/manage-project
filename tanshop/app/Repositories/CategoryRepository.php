<?php

namespace App\Repositories;

use App\Models\Category;

class CategoryRepository extends BaseRepository
{
    /**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return Category::class;
    }

    public function getParentCategory()
    {
        return $this->model->withParentIdNull()->orderby('name', 'asc')->get();
    }

    public function getParentCategoryWithout($id)
    {
        return $this->model->withParentIdNull()->withoutId($id)->orderby('name', 'asc')->get();
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['name'])->paginate(5);
    }
}
