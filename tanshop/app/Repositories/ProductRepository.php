<?php

namespace App\Repositories;

use App\Models\Product;

class ProductRepository extends BaseRepository
{
    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return Product::class;
    }

    public function all()
    {
        return $this->model->with('categories')->get();
    }

    public function find($id)
    {
        return $this->model->with('categories')->find($id);
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['name'])
            ->withPrice($dataSearch['price_ranger'])
            ->withCategoryName($dataSearch['category_name'])
            ->paginate(5);
    }
}
