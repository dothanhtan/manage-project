<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository extends BaseRepository
{
    /**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return User::class;
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['name'])
                            ->withEmail($dataSearch['email'])
                            ->withRoleName($dataSearch['role_name'])
                            ->paginate(10);
    }
}
