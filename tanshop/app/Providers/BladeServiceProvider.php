<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('convert', function ($money) {
            return "<?php echo number_format($money) ?>";
        });

        Blade::directive('active', function ($value) {
            return "<?php auth()->check() && (auth()->user()->hasRole($value) || auth()->user()->isAdmin()) ?>";;
        });
    }
}
