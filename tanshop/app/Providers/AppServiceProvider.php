<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::if('permission', function ($value)
        {
            return "<?php Auth::check() && (Auth::user()->hasPermission($value) || Auth::user()->isAdmin()); ?>";
        });

        Blade::if('role', function ($value)
        {
            return "<?php Auth::check() && (Auth::user()->hasRole($value) || Auth::user()->isAdmin()); ?>";
        });
    }
}
