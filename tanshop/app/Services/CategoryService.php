<?php

namespace App\Services;

use App\Repositories\CategoryRepository;

class CategoryService
{
    /**
     * @var $categoryRepository
     */
    protected $categoryRepository;

    /**
     * CategoryService constructor.
     *
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function getParentCategory()
    {
        return $this->categoryRepository->getParentCategory();
    }

    public function getAll()
    {
        return $this->categoryRepository->getAll();
    }

    public function search($request)
    {
        $dataSearch['name'] = $request->name ?? null;
        return $this->categoryRepository->search($dataSearch);
    }

    public function getParentCategoryWithout($id)
    {
        return $this->categoryRepository->getParentCategoryWithout($id);
    }

    public function findOrFail($id)
    {
        return $this->categoryRepository->findOrFail($id);
    }

    public function create($request)
    {
        return $this->categoryRepository->create($request->all());
    }

    public function update($request, $id)
    {
        return $this->categoryRepository->update($request->all(), $id);
    }

    public function delete($id)
    {
        return $this->categoryRepository->delete($id);
    }
}
