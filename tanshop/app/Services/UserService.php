<?php

namespace App\Services;

use App\Http\Requests\User\StoreUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Models\Role;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserService
{
    /**
     * @var $userRepository
     */
    protected $userRepository;

    /**
     * UserService constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function all()
    {
        return $this->userRepository->getAll();
    }

    public function findOrFail($id)
    {
        return $this->userRepository->findOrFail($id);
    }

    public function search($request)
    {
        $dataSearch['role_name'] = $request->role_name ?? null;
        $dataSearch['name'] = $request->name ?? null;
        $dataSearch['email'] = $request->email ?? null;
        return $this->userRepository->search($dataSearch);
    }

    public function create(StoreUserRequest $request)
    {
        $dataCreate = $request->all();
        $dataCreate['password'] = Hash::make($dataCreate['password']);
        $user = $this->userRepository->create($dataCreate);
        if (!empty($request->role))
        {
            $user->roles()->attach($dataCreate['role']);
        }
        return $user;
    }

    public function update($id, UpdateUserRequest $request)
    {
        $dataUpdate = $request->all();
        if (isset($dataUpdate['password']))
        {
            $dataUpdate['password'] = Hash::make($dataUpdate['password']);
        }
        else
        {
            unset($dataUpdate['password']);
        }
        $user = $this->userRepository->update($dataUpdate, $id);
        if (isset($request->role))
        {
            if (!empty($request->role))
            {
                $user->roles()->sync($request->role);
            }
            else
            {
                $user->roles()->detach();
            }
            return $user;
        }
    }

    public function delete($id)
    {
        return $this->userRepository->delete($id);
    }
}
