<?php

namespace App\Services;

use App\Repositories\RoleRepository;
use Illuminate\Http\Request;

class RoleService
{
    /**
     * @var $roleRepository
     */
    protected $roleRepository;

    /**
     * RoleService constructor.
     * @param RoleRepository $roleRepository
     */
    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function all()
    {
        return $this->roleRepository->getAll();
    }

    public function findOrFail($id)
    {
        return $this->roleRepository->findOrFail($id);
    }

    public function search($request)
    {
        $dataSearch['name'] = $request->name ?? null;
        return $this->roleRepository->search($dataSearch);
    }

    public function create(Request $request)
    {
        $dataCreate = $request->all();
        $role = $this->roleRepository->create($dataCreate);
        if (!empty($request->permission))
        {
            $role->permissions()->attach($dataCreate['permission']);
        }
        return $role;
    }

    public function update(Request $request, $id)
    {
        $dataUpdate = $request->all();
        $dataUpdate['permission'] = $request->permission ?? [];
        $role = $this->roleRepository->update($dataUpdate, $id);
        $role->syncPermission($dataUpdate['permission']);
        return $role;
    }

    public function delete(int $id)
    {
        return $this->roleRepository->delete($id);
    }
}
