<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = ['name', 'price', 'image', 'description'];

    /**
     * The categories that belong to the products.
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_product');
    }

    /**
     * Scope a query to search products by name.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'LIKE', '%' .$name . '%') : null;
    }

    /**
     * Scope a query to search products by price.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithPrice($query, $price)
    {
        return $price ? $query->where('price', '<=', $price) : null;
    }

    /**
     * Scope a query to search products by category.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithCategoryName($query, $categoryName)
    {
        return $categoryName ? $query->whereHas('categories', fn($query)
        => $query->where('name', 'LIKE', '%' .$categoryName . '%')) : null;
    }

    public function syncCategory($catID)
    {
        return $this->categories()->sync($catID);
    }
}
