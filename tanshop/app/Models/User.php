<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = ['name', 'phone', 'email', 'password'];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = ['password', 'remember_token',];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = ['email_verified_at' => 'datetime'];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function hasRole($roleName)
    {
        return $this->roles->contains('name', $roleName);
    }

    public function hasPermission($permissionName)
    {
        foreach ($this->roles as $role)
        {
            if ($role->hasPermission($permissionName))
            {
                return true;
            }
        }
        return false;
    }

    public function isAdmin()
    {
        return $this->hasRole('admin');
    }

    public function syncRole($roleID)
    {
        return $this->roles()->sync($roleID);
    }

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'LIKE', '%' . $name . '%') : null;
    }

    public function scopeWithEmail($query, $email)
    {
        return $email ? $query->where('email', 'LIKE', '%' . $email . '%') : null;
    }

    public function scopeWithRoleName($query, $roleName)
    {
        return $roleName ? $query->whereHas('roles', fn ($query) =>
        $query->where('name', 'LIKE', '%' . $roleName . '%')) : null;
    }
}
