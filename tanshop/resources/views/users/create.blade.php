@extends('layouts.theme')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="card">
                    <div class="card-header card-header-primary text-center">
                        <h4 class="card-title">Create New User</h4>
                    </div>
                    <form action="{{route('users.store')}}" method="post">
                        @csrf
                        <div class="card-body">
                            <div class="form-group mt-3">
                                <label for="name" class="bmd-label-floating">Name</label>
                                <input type="text" id="name" name="name" value="{{old('name')}}" class="form-control @error('name') is-invalid @enderror">
                                @error('name')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>

                            <div class="form-group mt-3">
                                <label for="phone" class="bmd-label-floating">Phone</label>
                                <input type="text" id="phone" name="phone" value="{{old('phone')}}" class="form-control @error('phone') is-invalid @enderror">
                                @error('phone')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>

                            <div class="form-group mt-3">
                                <label for="email" class="bmd-label-floating">Email</label>
                                <input type="email" id="email" name="email" value="{{old('email')}}" class="form-control @error('email') is-invalid @enderror">
                                @error('email')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>

                            <div class="form-group mt-3">
                                <label for="password" class="bmd-label-floating">Password</label>
                                <input type="password" id="password" name="password" class="form-control @error('password') is-invalid @enderror">
                                @error('password')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>

                            <div class="form-group mt-3">
                                <label for="repassword" class="bmd-label-floating">Confirm password</label>
                                <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" id="repassword" name="password_confirmation" placeholder="Confirm password">
                                @error('password_confirmation')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>

                            <div class="form-group mt-3">
                                @permission('')
                                <label for="role">Role</label>
                                <select class="selectpicker w-100" name="role[]" id="role" multiple>
                                    <option value="" selected>--Select--</option>
                                    @foreach($roles as $role)
                                        <option value="{{$role->id}}">{{$role->name}}</option>
                                    @endforeach
                                </select>
                                @endpermission
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a href="{{ route('users.index') }}" class="btn btn-secondary">Back</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
