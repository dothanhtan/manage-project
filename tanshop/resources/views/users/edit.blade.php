@extends('layouts.theme')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="card">
                    <div class="card-header card-header-primary text-center">
                        <h4 class="card-title">Edit User</h4>
                    </div>
                    <form action="{{route('users.update', $user->id)}}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="card-body">
                            <div class="form-group mt-3">
                                <label for="name" class="bmd-label-floating">Name</label>
                                <input type="text" id="name" name="name" value="{{old('name') ?? $user->name}}" class="form-control @error('name') is-invalid @enderror">
                                @error('name')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>

                            <div class="form-group mt-3">
                                <label for="phone" class="bmd-label-floating">Phone</label>
                                <input type="text" id="phone" name="phone" value="{{old('phone') ?? $user->phone}}" class="form-control @error('phone') is-invalid @enderror">
                                @error('phone')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>

                            <div class="form-group mt-3">
                                <label for="email" class="bmd-label-floating">Email</label>
                                <input type="email" id="email" name="email" value="{{old('email') ?? $user->email}}" class="form-control @error('email') is-invalid @enderror">
                                @error('email')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>

                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" id="changePassword" name="changePassword" class="custom-control-input">
                                <label for="changePassword" class="custom-control-label">Change Password</label>
                            </div>

                            <div class="form-group mt-3">
                                <label for="password" class="bmd-label-floating">Password</label>
                                <input type="password" id="password" name="password" class="form-control password @error('password') is-invalid @enderror" disabled="">
                                @error('password')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>

                            <div class="form-group mt-3">
                                <label class="bmd-label-floating" for="repassword">Confirm password</label>
                                <input type="password" name="password_confirmation" class="form-control password" id="repassword" disabled="">
                            </div>

                            <div class="form-group mt-3">
                                @permission('')
                                <label class="form-label" for="role">Role</label>
                                <select class="form-control" name="role" id="role">
                                @foreach($roles as $role)
                                    <option value="{{ $role->id }}" {{ ($user->roles->contains('id', $role->id)) ? 'selected' : '' }}>{{ $role->name }}</option>
                                @endforeach
                                </select>
                                @endpermission
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <a href="{{ route('users.index') }}" class="btn btn-secondary">Back</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('#changePassword').change(function() {
            if ($(this).is(":checked")) {
                $(".password").removeAttr('disabled');
            } else {
                $(".password").attr('disabled', '');
            }
        });
    </script>
@endsection
