@extends('layouts.theme')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="card">
                    <div class="card-header card-header-primary text-center">
                        <h4 class="card-title">Show User: {{$user->name}}</h4>
                    </div>
                    <form method="get">
                        <div class="card-body">
                            <div class="form-group mt-3 mb-4">
                                <label for="name" class="bmd-label-floating">Name</label>
                                <input type="text" id="name" name="name" value="{{$user->name}}" class="form-control text-info" readonly>
                            </div>

                            <div class="form-group mb-4">
                                <label for="phone" class="bmd-label-floating">Phone</label>
                                <input type="text" id="phone" name="phone" value="{{$user->phone}}" class="form-control text-info" readonly>
                            </div>

                            <div class="form-group">
                                <label for="email" class="bmd-label-floating">Email</label>
                                <input type="email" id="email" name="email" value="{{$user->email}}" class="form-control text-info" readonly>
                            </div>
                        </div>

                        <div class="card-footer">
                            <a href="{{route('users.index')}}" class="btn btn-default">BACK <i class="fas fa-arrow-right"></i></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
