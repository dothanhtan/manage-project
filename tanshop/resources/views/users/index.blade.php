@extends('layouts.theme')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-md-12">
                @if(session('message'))
                    <div class="alert alert-success">
                        <strong>{{session('message')}}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-plain">
                    <div class="card-header card-header-primary">
                        <h3 class="card-title text-center mt-0">USERS LIST</h3>
                    </div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center my-3">
                            <div>
                                @permission('user-create')
                                <a href="{{route('users.create')}}" class="btn btn-sm btn-primary"><i class="fas fa-plus"></i> ADD</a>
                                @endpermission
                            </div>

                            <div class="card-tools">
                                <form action="{{ route('users.index') }}" method="GET" class="form-inline">
                                    <div class="form-group">
                                        <input type="text" name="name" id="name" class="form-control" placeholder="Name" value="{{ $_GET['name'] ?? '' }}"/>
                                    </div>
                                    <div class="form-group mx-sm-3">
                                        <input type="email" name="email" id="email" class="form-control" placeholder="Email" value="{{ $_GET['email'] ?? '' }}"/>
                                    </div>
                                    <div class="form-group">
                                        <select name="role_name" id="role_name" class="form-control">
                                            <option value="">Select Role</option>
                                            @foreach ($roles as $role)
                                                <option value="{{ $role->name }}" {{ (isset($_GET['role_name']) && ($_GET['role_name'] == $role->name)) ? 'selected' : '' }}>{{ $role->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group ml-3">
                                        <button class="btn btn-sm btn-info" type="submit"><i class="fas fa-search"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Roles</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                @foreach($users as $user)
                                <tbody>
                                    <tr>
                                        <td>
                                            {{$user->id}}
                                        </td>
                                        <td>
                                            {{$user->name}}
                                        </td>
                                        <td>
                                            {{$user->phone}}
                                        </td>
                                        <td>
                                            {{$user->email}}
                                        </td>
                                        <td>
                                            @foreach($user->roles as $role)
                                                <span class="badge rounded-pill bg-info text-dark">{{$role->name}}</span>
                                            @endforeach
                                        </td>
                                        <td class="d-flex">
                                            @permission('user-list')
                                            <div class="mr-2">
                                                <a href="{{route('users.show', $user->id)}}" class="btn btn-info btn-sm"><i class="fas fa-eye"></i></a>
                                            </div>
                                            @endpermission

                                            @permission('user-edit')
                                            <div class="mr-2">
                                                <a href="{{route('users.edit', $user->id)}}" class="btn btn-warning btn-sm"><i class="fas fa-pencil"></i></a>
                                            </div>
                                            @endpermission

                                            @permission('user-delete')
                                            <form id="deleteForm{{$user->id}}" action="{{route('users.destroy', $user->id)}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                            <button data-form="deleteForm{{$user->id}}" class="btn btn-delete btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                            @endpermission
                                        </td>
                                    </tr>
                                </tbody>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{ $users->withQueryString()->links('pagination::bootstrap-4') }}
    </div>
@endsection

@section('script')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        $(function (){
            $(document).on('click', '.btn-delete', function () {
                let formID = $(this).data('form')
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $(`#${formID}`).submit()
                        Swal.fire(
                            'Deleted!',
                            'This user has been deleted.',
                            'success'
                        )
                    }
                })
            })
        })
    </script>
@endsection
