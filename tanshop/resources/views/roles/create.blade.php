@extends('layouts.theme')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card">
                <div class="card-header card-header-primary text-center">
                    <h4 class="card-title">Create New Role</h4>
                </div>
                <form action="{{route('roles.store')}}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="form-group mt-3">
                            <label for="name" class="bmd-label-floating">Name</label>
                            <input type="text" id="name" name="name" value="{{old('name')}}" class="form-control @error('name') is-invalid @enderror">
                            @error('name')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>

                        <div class="form-group mt-3">
                            <label for="display_name" class="bmd-label-floating">Display Name</label>
                            <input type="text" id="display_name" name="display_name" value="{{old('display_name')}}" class="form-control @error('display_name') is-invalid @enderror">
                            @error('display_name')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>

                        <div class="form-row mt-3">
                            <div class="col-md-12">
                                <label for="permission">Choose permissions</label>
                            </div>
                            @foreach($permissions->split($permissions->count()/2) as $row)
                            <div class="col-md-6">
                                @foreach($row as $permission)
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" value="{{ $permission->id }}" name="permission[]" type="checkbox" id="{{ $permission->name }}" >
                                    <label for="{{ $permission->name }}" class="custom-control-label">{{ $permission->name }}</label>
                                </div>
                                @endforeach
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary mr-2">Save</button>
                        <a href="{{ route('roles.index') }}" class="btn btn-secondary">Back</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
