@extends('layouts.theme')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-md-12">
                @if(session('message'))
                    <div class="alert alert-success">
                        <strong>{{session('message')}}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-plain">
                    <div class="card-header card-header-primary">
                        <h3 class="card-title text-center mt-0">ROLES LIST</h3>
                    </div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            @permission('role-create')
                            <div>
                                <a href="{{route('roles.create')}}" class="btn btn-sm btn-info my-3"><i class="fas fa-plus"></i> ADD</a>
                            </div>
                            @endpermission

                            <form class="mt-3" action="{{ route('roles.index') }}" method="GET">
                                <div class="input-group mb-3">
                                    <input type="text" name="name" class="form-control" placeholder="Name..." value="{{ $_GET['name'] ?? '' }}">
                                    <div class="input-group-append">
                                        <button class="btn btn-sm btn-info" type="submit"><i class="fas fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead class="">
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Display Name</th>
                                    <th>Action</th>
                                </thead>
                                @foreach($roles as $role)
                                <tbody>
                                    <tr>
                                        <td>
                                            {{$role->id}}
                                        </td>
                                        <td>
                                            {{$role->name}}
                                        </td>
                                        <td>
                                            {{$role->display_name}}
                                        </td>
                                        <td class="d-flex">
                                            @permission('role-list')
                                            <div class="mr-2">
                                                <a href="{{route('roles.show', $role->id)}}" class="btn btn-info btn-sm"><i class="fas fa-eye"></i></a>
                                            </div>
                                            @endpermission

                                            @permission('role-edit')
                                            <div class="mr-2">
                                                <a href="{{route('roles.edit', $role->id)}}" class="btn btn-warning btn-sm"><i class="fas fa-pencil"></i></a>
                                            </div>
                                            @endpermission

                                            <form id="deleteForm{{$role->id}}" action="{{route('roles.destroy', $role->id)}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                            @permission('role-delete')
                                            <button data-form="deleteForm{{$role->id}}" class="btn btn-delete btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                            @endpermission
                                        </td>
                                    </tr>
                                </tbody>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{ $roles->withQueryString()->links('pagination::bootstrap-4') }}
    </div>
@endsection

@section('script')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        $(function (){
            $(document).on('click', '.btn-delete', function () {
                let formID = $(this).data('form')
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $(`#${formID}`).submit()
                        Swal.fire(
                            'Deleted!',
                            'This user has been deleted.',
                            'success'
                        )
                    }
                })
            })
        })
    </script>
@endsection
