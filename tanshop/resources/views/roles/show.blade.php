@extends('layouts.theme')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="card">
                    <div class="card-header card-header-primary text-center">
                        <h4 class="card-title">Show Role</h4>
                    </div>
                    <form method="get">
                        <div class="card-body">
                            <div class="form-group mt-3 mb-4">
                                <label for="name" class="bmd-label-floating">Name</label>
                                <input type="text" id="name" name="name" value="{{$role->name}}" class="form-control text-info" readonly>
                            </div>

                            <div class="form-group mb-4">
                                <label for="display_name" class="bmd-label-floating">Display Name</label>
                                <input type="text" id="display_name" name="display_name" value="{{$role->display_name}}" class="form-control text-info" readonly>
                            </div>

                            <div class="form-row">
                                <div class="col-md-12">
                                    <label for="permission">Choose permissions</label>
                                </div>
                                @foreach($permissions->split($permissions->count()/2) as $row)
                                    <div class="col-md-6">
                                        @foreach($row as $permission)
                                            <div class="custom-control custom-checkbox">
                                                <input class="custom-control-input" {{ ($role->permissions->contains('id', $permission->id)) ? 'checked' : '' }} value="{{ $permission->id }}" name="permission_id[]" type="checkbox" id="{{ $permission->name }}" disabled>
                                                <label for="{{ $permission->name }}" class="custom-control-label">{{ $permission->name }}</label>
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="card-footer">
                            <a href="{{ route('roles.index') }}" class="btn btn-secondary">Back <i class="fas fa-arrow-right"></i></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
