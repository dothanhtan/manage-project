<!DOCTYPE html>
<html lang="en">
    <head>
        @include('layouts.header')
    </head>
    <body class="dark-edition">
        <div class="wrapper">
            @include('layouts.sidebar')
            @include('layouts.main')
        </div>
        <div class="library">
            @include('layouts.library')
        </div>
    </body>
</html>
