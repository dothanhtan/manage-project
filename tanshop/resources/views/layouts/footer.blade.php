<div class="container-fluid">
    <nav class="float-left">
        <ul>
            <li>
                <a href="#">DEHA ACADEMY</a>
            </li>
            <li>
                <a href="#">About Us</a>
            </li>
            <li>
                <a href="#">Blog</a>
            </li>
            <li>
                <a href="#">Licenses</a>
            </li>
        </ul>
    </nav>
    <div class="copyright float-right" id="date">
        , made with <i class="material-icons">favorite</i> by
        <a href="#" target="_blank">Thanh Tan</a> for a better web.
    </div>
</div>
