<div class="sidebar" data-color="purple" data-background-color="black" data-image="{{asset('assets/img/sidebar-2.jpg')}}">
    <div class="logo">
        <a href="{{route('home')}}" class="simple-text logo-normal">DEHA ACADEMY</a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            @guest
                @if (Route::has('home'))
                    <li class="nav-item active">
                        <a class="nav-link" href="{{route('home')}}">
                            <i class="material-icons">dashboard</i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                @endif
            @else
                @if (Auth::user()->hasPermission('user-list'))
                    <li class="nav-item ">
                        <a class="nav-link" href="{{route('users.index')}}">
                            <i class="material-icons">person</i>
                            <p>User Manager</p>
                        </a>
                    </li>
                @endif

                @if (Auth::user()->hasPermission('role-list'))
                    <li class="nav-item ">
                        <a class="nav-link" href="{{route('roles.index')}}">
                            <i class="material-icons">content_paste</i>
                            <p>Role Manager</p>
                        </a>
                    </li>
                @endif

                @if (Auth::user()->hasPermission('category-list'))
                    <li class="nav-item ">
                        <a class="nav-link" href="{{route('categories.index')}}">
                            <i class="material-icons">library_books</i>
                            <p>Category Manager</p>
                        </a>
                    </li>
                @endif

                @if (Auth::user()->hasPermission('product-list'))
                    <li class="nav-item ">
                        <a class="nav-link" href="{{route('products.index')}}">
                            <i class="material-icons">bubble_chart</i>
                            <p>Product Manager</p>
                        </a>
                    </li>
                @endif
            @endguest
        </ul>
    </div>
</div>
