<div class="main-panel">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top" id="navigation-example">
        @include('layouts.menu')
    </nav>
    <!-- End Navbar -->

    <!-- Content -->
    <div class="content">
        @yield('content')
    </div>
    <!-- End Content -->

    <!-- Footer -->
    <footer class="footer">
        @include('layouts.footer')
    </footer>
    <!-- End Footer -->
</div>
