@extends('layouts.theme')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-md-12">
                @if(session('success'))
                    <div class="alert alert-success">
                        <strong>{{session('success')}}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card card-plain">
                    <div class="card-header card-header-primary">
                        <h3 class="card-title text-center mt-0">PRODUCTS LIST</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center my-3">
                            <div>
                                @permission('product-create')
                                <button type="button" name="create_product" id="create_product" data-route-store="{{ route('products.store') }}" class="btn btn-primary btn-sm create_product"><i class="fas fa-plus"></i> ADD</button>
                                @endpermission
                            </div>
                            <div class="card-tools">
                                <form action="{{ route('products.index') }}" method="GET" id="list-products" class="form-inline">
                                    <div class="form-group">
                                        <input type="number" class="form-control" id="price_ranger" name="price_ranger" min="0" max="60000000" value="{{$_GET['price_ranger'] ?? 0 }}" placeholder="Price">
                                    </div>

                                    <div class="form-group mx-sm-3">
                                        <input type="text" name="name" id="search_name" class="form-control" placeholder="Name" value="{{ $_GET['name'] ?? '' }}">
                                    </div>

                                    <div class="form-group">
                                        <select name="category_name" id="category_name" class="form-control">
                                            <option value="">Select Category</option>
                                            @foreach ($categories as $category)
                                                <option value="{{ $category->name }}" {{ (isset($_GET['category_name']) && ($_GET['category_name'] == $category->name)) ? 'selected' : '' }}>{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group ml-3">
                                        <button type="submit" class="btn btn-default btn-sm"><i class="fas fa-search"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div id="data_table">
                            <table id="user_table" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Image</th>
                                        <th>Name</th>
                                        <th>Price</th>
                                        <th>Description</th>
                                        <th>Category</th>
                                        <th>Options</th>
                                    </tr>
                                </thead>
                                @foreach ($products as $product)
                                <tbody>
                                    <tr id="product_id_{{ $product->id }}">
                                        <td>
                                            {{ $product->id }}
                                        </td>
                                        <td>
                                            <img src="{{ '/images/' . $product->image }}" class="img-fluid" width="150" height="150" alt="image_default.png">
                                        </td>
                                        <td>
                                            {{ $product->name }}
                                        </td>
                                        <td>
                                            @convert($product->price) VNĐ
                                        </td>
                                        <td>
                                            {{ $product->description }}
                                        </td>
                                        <td>
                                            @foreach($product->categories as $category)
                                                <span class="badge rounded-pill bg-info text-dark">{{ $category->name }}</span>
                                            @endforeach
                                        </td>
                                        <td>
                                            @permission('product-edit')
                                            <a href="javascript:void(0)" data-id="{{ $product->id }}" data-toggle="modal" data-target="#formModal" data-route-edit="{{ route('products.edit', $product->id) }}" id="edit-product" data-route-update="{{ route('products.update', $product->id) }}" class="btn btn-warning btn-sm edit-product"><i class="fas fa-pencil"></i></a>
                                            @endpermission

                                            @permission('product-delete')
                                            <form id="deleteForm{{$product->id}}" method="POST" >
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                            <a href="javascript:void(0)" data-id="{{ $product->id }}" data-form="deleteForm{{$product->id}}" data-route-destroy="{{ route('products.destroy', $product->id) }}" id="delete-product" class="btn btn-danger btn-sm btn-delete delete-product"><i class="fas fa-trash"></i></a>
                                            @endpermission
                                        </td>
                                    </tr>
                                </tbody>
                                @endforeach
                            </table>
                            {{ $products->withQueryString()->links('pagination::bootstrap-4') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('products.modal')

@endsection

@section('script')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('/assets/js/product/module.js') }}"></script>
    <script src="{{ asset('/assets/js/product/script.js') }}"></script>
    <script>
        $(function ()
        {
            $(document).on('click', '.btn-delete', function ()
            {
                let formID = $(this).data('form')
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $(`#${formID}`).submit()
                        Swal.fire(
                            'Deleted!',
                            'This product has been deleted.',
                            'success'
                        )
                    }
                })
            })
        })
    </script>
@endsection
