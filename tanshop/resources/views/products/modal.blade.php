<div id="formModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title">Add New Product</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
            </div>

            <form method="POST" id="formSave" class="form-horizontal text-dark" enctype="multipart/form-data">
                @csrf
                <div class="modal-body bg-rose">
                    <div class="form-group">
                        <input type="hidden" name="id" id="id" class="form-control @error('id') is-invalid @enderror"/>
                        @error('id')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="name" class="text-primary">Name</label>
                        <input type="text" name="name" id="name" class="form-control text-dark @error('name') is-invalid @enderror"/>

                        @error('name')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="price" class="text-primary">Price</label>
                        <input type="number" name="price" id="price" class="form-control text-dark @error('price') is-invalid @enderror"/>

                        @error('price')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="description" class="text-primary">Description</label>
                        <textarea id="description" name="description" rows="3" class="form-control text-dark @error('description') is-invalid @enderror"></textarea>

                        @error('description')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="category_id" class="text-primary">Category</label>
                        <select id="category_id" name="category_id[]" class="selectpicker w-100 @error('category_id') is-invalid @enderror" multiple>
                            <option value="" selected>None</option>
                            @if($categories)
                                @foreach($categories as $value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                @endforeach
                            @endif
                        </select>
                        @error('category_id')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="image" class="text-primary">Select Image</label>
                        <input type="file" name="image" id="image" class="form-control text-dark @error('image') is-invalid @enderror"/>
                        <span id="store_image"></span>

                        @error('image')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="action" id="action" class="action" />
                    <input type="hidden" name="hidden_id" id="hidden_id" />
                    <input type="submit" name="action_button" id="action_button" class="btn btn-primary" value="Add" />
                    <a href="{{route('products.index')}}" class="btn btn-default">Back</a>
                </div>
            </form>
        </div>
    </div>
</div>
