@extends('layouts.theme')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="card">
                    <div class="card-header card-header-primary text-center">
                        <h4 class="card-title">Show Category</h4>
                    </div>
                    <form method="get">
                        <div class="card-body">
                            <div class="form-group mt-3">
                                <label for="name">Category name</label>
                                <input type="text" id="name" name="name" value="{{$category->name}}" class="form-control" disabled>
                            </div>

                            <div class="form-group mt-3">
                                <label for="parent_id">Parent category</label>
                                <input type="text" id="parent_id" name="parent_id" value="{{ $category->parent->name ?? 'None' }}" class="form-control" disabled>
                            </div>
                        </div>
                        <div class="card-footer">
                            <a href="{{ route('categories.index') }}" class="btn btn-secondary">Back <i class="fas fa-arrow-right"></i></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
