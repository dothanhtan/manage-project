@extends('layouts.theme')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col col-md-12">
            @if(session('message'))
                <div class="alert alert-success">
                    <strong>{{session('message')}}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card card-plain">
                <div class="card-header card-header-primary">
                    <h3 class="card-title text-center mt-0">CATEGORIES LIST</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <div>
                            @permission('category-create')
                            <a class="btn btn-info btn-sm add-new" href="{{ route('categories.create') }}">
                                <i class="fas fa-plus"></i> ADD
                            </a>
                            @endpermission
                        </div>

                        <div class="card-tools">
                            <form action="{{ route('categories.index') }}" method="GET">
                                <div class="input-group mb-3">
                                    <input type="text" name="name" class="form-control" placeholder="Name..." value="{{ $_GET['name'] ?? '' }}">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default btn-sm">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-hover" id="example">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Category Name</th>
                                    <th>Parent Category</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            @if(isset($categories))
                            <?php $_SESSION['i'] = 0; ?>
                            @foreach($categories as $category)
                            @php $_SESSION['i'] = $_SESSION['i'] + 1; @endphp
                            <tbody>
                                <tr>
                                    <td>
                                        {{ $category->id  }}
                                    </td>
                                    <td>
                                        {{ $category->name }}
                                    </td>
                                    <td>
                                        {{ $category->parent->name ?? 'None' }}
                                    </td>
                                    <td class="d-flex">
                                        <div class="mr-2">
                                            @permission('category-list')
                                            <a href="{{ route('categories.show', $category->id) }}" class="btn btn-info btn-sm"><i class="fas fa-eye"></i></a>
                                            @endpermission
                                        </div>

                                        <div class="mr-2">
                                            @permission('category-edit')
                                            <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-warning btn-sm"><i class="fas fa-pencil"></i></a>
                                            @endpermission
                                        </div>

                                        @permission('category-delete')
                                        <form id="deleteForm{{$category->id}}" action="{{ route('categories.destroy', $category->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                        <button type="submit" data-form="deleteForm{{$category->id}}" class="btn btn-danger btn-sm btn-delete"><i class="fas fa-trash"></i></button>
                                        @endpermission
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                            <?php unset($_SESSION['i']); ?>
                            @endif
                        </table>
                        {{ $categories->withQueryString()->links('pagination::bootstrap-4') }}
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
        </div>

    </div>
</div>
@endsection

@section('script')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        $(function (){
            $(document).on('click', '.btn-delete', function () {
                let formID = $(this).data('form')
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $(`#${formID}`).submit()
                        Swal.fire(
                            'Deleted!',
                            'This user has been deleted.',
                            'success'
                        )
                    }
                })
            })
        })
    </script>
@endsection
